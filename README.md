# zWorld is somewhat of a virtual world, a virtual room where users can walk around in with an administration interface.
# License: zWorld is available under the Creative Commons -- Share Alike Attribution Non-Commercial license. Yup, that's right, there's finally a stable version!
# Running/Modifying Source Code: The client runs in Scratch, and the server runs in Panther. You'll need Mesh enabled, and all the clients and the server connected to the same network.
# Panther/Scratch websites:
# scratch.mit.edu
# pantherprogramming.weebly.com